# Description

This is an Angular tutorial that can be used to learn the basic as well as the advanced techniques of Angular.

Every Git tag represents a learning step; begin by checking out `step-1` (`git checkout step-1`), run it and feel free to edit to learn more.
Proceed to `step-2` and so on when you feel ready.

Remember that each step may introduce new files and new dependencies; the first are usually generated with the `ng` command (see `TOPICS.txt`); the latter may be installed if any changes are introduced to `package.json`.

# Requirements 

Node 12 is absolutely required as this project depends on Angular 8 (which depends on Node 12).

This project also relies on [this backend][1]. Please refer to its page to understand how to download it and run it.

# Installation and run

Clone the project and run it:

```bash
$ git clone git@git-maps.maps1.mapsengineering.com:fast/maps-portal.git
$ cd maps-portal
maps-portal$ npm i
maps-portal$ npm start
```

This will start the development server at the usual address: http://localhost:4200.

# Useful tips

## TOPICS.txt

File `TOPICS.txt` contains the ordered sequence of commands that must be run in order to proceed from one step to another. There's no explicit reference to the exact step, but you can guess it from the git command.

## Step-1

Once the project is installed and is up and running you shoudl go back to `step-1`:

```bash
maps-portal$ git checkout step-1
```

This will revert everything to the initial step.

## Explore the history of Git

This command can ease the visualization of the Git log (you should make an alias out of it):

```bash
maps-portal$ git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'
```

## Check differences between steps

There're different ways to understand what has been changed from one step to another:
* get a list of the modified files:

    ```bash
    maps-portal$ git diff step-N step-N+1 --name-only
    # or
    maps-portal$ git diff step-N step-N+1 --stat
    ```

* get the difference in patch format:

    ```bash
    # -p can be omitted as it should be the default
    maps-portal$ git diff step-N step-N+1 -p
    ```

* get the difference of a specific file:

    ```bash
    # change package.json with any other file
    maps-portal$ git diff step-N step-N+1 -- package.json
    ```


[1]: https://git-maps.maps1.mapsengineering.com/fast/maps-portal