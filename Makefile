VERSION := $(shell node ./version.js)

all: init build

init:
	npm install

lint:
	./node_modules/.bin/ng lint

build:
	./node_modules/.bin/ng build --prod --aot --base-href /
	cd ./dist/maps-portal && tar zcf ../../maps-portal-$(VERSION).tgz *

clean:
	$(RM) -fr ./dist *.tgz

clean-all: clean
	$(RM) -fr ./node_modules

.PHONY: all init lint build clean clean-all
