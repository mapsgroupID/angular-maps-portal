import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BillOfMaterialsModule } from './bill-of-materials.module';
import { EmployeeCardComponent } from './employee-card/employee-card.component';
import { CommunicationVerbosePipe } from './communication-verbose.pipe';
import { AgeHighlighterDirective } from './age-highlighter.directive';
import { EmployeeService } from './employee.service';
import { CommunicationsCardComponent } from './communications-card/communications-card.component';
import { CommunicationsService } from './communications.service';
import { EmployeeEditorComponent } from './employee-editor/employee-editor.component';
import { EmployeesComponent } from './views/employees/employees.component';
import { EmployeeWizComponent } from './views/employee-wiz/employee-wiz.component';
import { EmployeeComponent } from './views/employee/employee.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeCardComponent,
    CommunicationVerbosePipe,
    AgeHighlighterDirective,
    CommunicationsCardComponent,
    EmployeeEditorComponent,
    EmployeesComponent,
    EmployeeWizComponent,
    EmployeeComponent],
  imports: [
    // angular modules
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    // application modules
    BillOfMaterialsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
