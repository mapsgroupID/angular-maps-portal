import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorizationService } from './authorization.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeEditorGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly auth: AuthorizationService) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree {
    const canI = this.auth.canI('create-employee');
    if (!canI) {
      console.error(`I cannot create a new employee`);
      return this.router.parseUrl(this.router.url);
    }
    return canI;
  }

}
