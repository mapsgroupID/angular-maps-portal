import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { CommunicationCounts } from '../dto';
import { CommunicationsService } from '../communications.service';

@Component({
  selector: 'app-communications-card',
  templateUrl: './communications-card.component.html',
  styleUrls: ['./communications-card.component.css']
})
export class CommunicationsCardComponent implements OnDestroy {
  private readonly subscription: Subscription;
  communications: CommunicationCounts[];

  constructor(private readonly commService: CommunicationsService) {
    this.communications = [];
    this.subscription = commService.subscribe().subscribe(cc => {
      this.update(cc);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  trackByRecipientId(idx: number, cc: CommunicationCounts) {
    return cc.recipient.id;
  }

  private update(cc: CommunicationCounts) {
    const idx = this.communications.findIndex(c => c.recipient.id === cc.recipient.id);
    if (idx < 0) {
      this.communications = [cc, ...this.communications];
    } else {
      const pre = this.communications.slice(0, idx);
      const post = this.communications.slice(idx + 1);
      this.communications = [cc, ...pre, ...post];
    }
  }
}
