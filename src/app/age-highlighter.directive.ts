import { Directive, Input, HostListener, ElementRef, OnInit } from '@angular/core';

const DEFAULT_COLOR = '#FF7E75';
const DEFAULT_LEVELS = {
  30: '#75FFC3',
  35: '#75FF7E',
  40: '#B1FF75',
  50: '#F6FF75',
  60: '#FFC375',
  70: '#FF7E75'
};

@Directive({
  selector: '[appAgeHighlighter]'
})
export class AgeHighlighterDirective implements OnInit {
  @Input('appAgeHighlighter') colors: { [level: string]: string };

  levels: number[];

  constructor(private readonly el: ElementRef) { }

  ngOnInit() {
    let keys;
    if (this.colors && Object.keys(this.colors).length) {
      keys = Object.keys(this.colors);
    } else {
      keys = Object.keys(DEFAULT_LEVELS);
    }
    this.levels = keys.sort((c1, c2) => parseInt(c1, 10) - parseInt(c2, 10)).map(c => parseInt(c, 10));
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight(this.findColor());
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight();
  }

  private findColor(): string {
    const txt = this.el.nativeElement.textContent;
    const age = parseInt(txt, 10);
    let color = DEFAULT_COLOR;
    for (const l of this.levels) {
      if (age < l) {
        if (this.colors[l]) {
          color = this.colors[l];
          break;
        } else if (DEFAULT_LEVELS[l]) {
          color = DEFAULT_LEVELS[l];
          break;
        }
      }
    }
    return color;
  }

  private highlight(color?: string) {
    if (color) {
      this.el.nativeElement.style.backgroundColor = color;
    } else {
      this.el.nativeElement.style.backgroundColor = 'transparent';
    }
  }

}
