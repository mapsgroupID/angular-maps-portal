import { Injectable } from '@angular/core';
import { CommunicationCounts, Communication, Channel } from './dto';
import { Subject, Observable, concat } from 'rxjs';

function cloneCounts(counts: { [channel: string]: number }): { [channel: string]: number } {
  const out: { [channel: string]: number } = {};
  for (const k of Object.keys(counts)) {
    out[k] = counts[k];
  }
  return out;
}

function clone(cc: CommunicationCounts): CommunicationCounts {
  return {
    counts: cloneCounts(cc.counts),
    recipient: cc.recipient // recipient not cloned 'cause i'm lazy
  };
}

@Injectable({
  providedIn: 'root'
})
export class CommunicationsService {
  communications: CommunicationCounts[] = [];
  private readonly subject: Subject<CommunicationCounts>;

  constructor() {
    this.subject = new Subject();
  }

  receive(inComm: Communication) {
    let cc = this.communications.find(x => x.recipient.id === inComm.recipient.id);
    if (!cc) {
      const counts: { [ch: string]: number } = {};
      counts[Channel.EMAIL] = 0;
      counts[Channel.PHONE] = 0;
      cc = { counts, recipient: inComm.recipient };
      this.communications.push(cc);
    }
    cc.counts[inComm.channel]++;
    this.subject.next(clone(cc));
  }

  subscribe(): Observable<CommunicationCounts> {
    const obs: Observable<CommunicationCounts> = new Observable(subscriber => {
      this.communications.forEach(c => subscriber.next(c));
      subscriber.complete();
    });

    return concat(obs, this.subject);
  }
}
