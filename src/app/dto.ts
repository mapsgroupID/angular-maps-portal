export function randomlyIndex(arr) {
  return () => Math.floor(Math.random() * arr.length);
}

export function randomly(arr) {
  const anIndex = randomlyIndex(arr);
  return () => {
    return arr[anIndex()];
  };
}

export function trueOrFalse() {
  return Math.floor(Math.random() * 2) === 1;
}

const NAMES = [
  ['Linus', 'Torvalds'],
  ['Andrew', 'Tanenbaum'],
  ['Edsger', 'Dijkstra'],
  ['Richard', 'Stallman'],
  ['Dennis', 'Ritchie'],
  ['Brian', 'Kernighan']
];

export enum Position {
  DEV = 'Software developer',
  ARCH = 'Software architect',
  PM = 'Project manager',
  RES = 'Researcher',
  CTO = 'CTO'
}

export interface Employee {
  id: number;
  avatarUrl: string;
  firstName: string;
  lastName: string;
  position: Position;
  description: string;
  age: number;
  hasEmail: boolean;
  hasPhone: boolean;
}

const AVATARS = [
  '/static/wonder-woman.jpg',
  '/static/superman.jpg',
  '/static/batman.jpg',
  '/static/batgirl.jpg',
  '/static/joker.jpg',
  '/static/penguin.jpg',
  '/static/two-face.jpg',
  '/static/robin.jpg'
];

export const aPosition = randomly([
  Position.DEV,
  Position.ARCH,
  Position.PM,
  Position.RES,
  Position.CTO
]);

/**
 * Creates a function that emits sequences of numbers when invoked.
 *
 * @param  base The base of the sequence, default is 1
 * @return A function that generates sequence numbers
 */
export function sequence(base = 1): () => number {
  let no = base;
  return () => no++;
}

export const aName = randomly(NAMES);
export const anAvatar = randomly(AVATARS);
export const anAge = () => 20 + Math.floor(Math.random() * 60);

const anEmployeeId = sequence(1);

export function anEmployee(): Employee {
  const [f, l] = aName();
  return {
    id: anEmployeeId(),
    avatarUrl: anAvatar(),
    age: anAge(),
    description: 'Bla bla bla bla... bla bla bla.',
    firstName: f,
    lastName: l,
    position: aPosition(),
    hasEmail: trueOrFalse(),
    hasPhone: trueOrFalse()
  };
}

export enum Channel {
  EMAIL,
  PHONE
}

export interface Communication {
  channel: Channel;
  recipient: Employee;
}

export interface CommunicationCounts {
  counts: { [channel: string]: number };
  recipient: Employee;
}
