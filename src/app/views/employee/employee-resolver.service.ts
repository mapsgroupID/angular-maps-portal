import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Employee } from 'src/app/dto';
import { EmployeeService } from 'src/app/employee.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeResolverService implements Resolve<Employee> {

  constructor(
    private readonly empService: EmployeeService,
    private readonly router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Employee | Observable<Employee> | Promise<Employee> {
    const empId = route.params.id;
    return this.empService.load(empId).pipe(
      catchError(err => {
        console.error(`Cannot load employee ${empId}`, err);
        this.router.navigate([this.router.url]);
        return EMPTY;
      })
    );
  }
}
