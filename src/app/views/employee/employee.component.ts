import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject, merge } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { CommunicationsService } from '../../communications.service';
import { Communication, Employee } from '../../dto';
import { EmployeeService } from '../../employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent {
  refresher: Subject<number>;
  employee: Observable<Employee>;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly empService: EmployeeService,
    private readonly commService: CommunicationsService) {
    // remember that a subject is also an observable seen from the consumer side
    this.refresher = new Subject();

    // the routed data is transformed as usual
    const obs1 = route.data.pipe(
      map((data: { employee: Employee }) => data.employee)
    );

    // any id emitted by `refresher` is transformed into an observable of Employee
    const obs2 = this.refresher.pipe(mergeMap(id => this.empService.load(id)));

    // both observables are combined, whatever arrives first is returned
    this.employee = merge(obs1, obs2);
  }

  communication(inComm: Communication) {
    this.commService.receive(inComm);
  }

  refresh(id) {
    this.refresher.next(id);
  }
}
