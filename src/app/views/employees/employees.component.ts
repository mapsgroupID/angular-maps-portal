import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { CommunicationsService } from 'src/app/communications.service';
import { Channel, Communication, Employee } from 'src/app/dto';
import { EmployeeService } from 'src/app/employee.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent {
  employees: Observable<Employee[]>;
  Channel = Channel;

  constructor(
    private readonly empService: EmployeeService,
    private readonly commService: CommunicationsService) {
    this.employees = empService.listAll();
  }

  trackById(index: number, employee: Employee) {
    return employee.id;
  }

  communication(inComm: Communication) {
    this.commService.receive(inComm);
  }
}
