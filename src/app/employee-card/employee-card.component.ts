import {
  Component,
  Input,
  DoCheck,
  KeyValueDiffers,
  OnInit,
  KeyValueDiffer,
  KeyValueChanges,
  Output,
  EventEmitter
} from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Employee, Position, Communication, Channel } from '../dto';

@Component({
  selector: 'app-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.css'],
  animations: [
    trigger('glow', [
      state(
        'stable',
        style({
          boxShadow: '0px 0px 0px 0px rgba(245,235,101,1)'
        })
      ),
      state(
        'changed',
        style({
          boxShadow: '0px 0px 23px 15px rgba(245,235,101,1)'
        })
      ),
      transition('stable <=> changed', [animate(500)])
    ])
  ]
})
export class EmployeeCardComponent implements OnInit, DoCheck {
  @Input() employee: Employee;
  @Output() communicate: EventEmitter<Communication>;
  Position = Position;
  Channel = Channel;
  changed = false;
  messages: { [c: string]: number } = {};
  levels = {
    40: '#75FFFB',
    50: '#75BEFF',
    60: '#7579FF',
    70: '#B675FF',
    80: '#FB75FF',
    90: '#FF75BE'
  };

  private differ: KeyValueDiffer<string, any>;

  constructor(private readonly differs: KeyValueDiffers) {
    this.communicate = new EventEmitter();
  }

  ngOnInit() {
    this.differ = this.differs.find(this.employeeMap).create();
  }

  ngDoCheck() {
    const changes: KeyValueChanges<string, any> = this.differ.diff(this.employeeMap);
    if (changes) {
      changes.forEachChangedItem(record => {
        this.changed = true;
      });
    }
  }

  private get employeeMap() {
    return {
      a: this.employee.age,
      u: this.employee.avatarUrl,
      n: `${this.employee.firstName} ${this.employee.lastName}`,
      d: this.employee.description,
      p: this.employee.position,
      E: this.employee.hasEmail,
      P: this.employee.hasPhone
    };
  }

  stopGlowing(event: AnimationEvent) {
    this.changed = false;
  }

  communicateBy(channel: Channel) {
    this.communicate.emit({ channel, recipient: this.employee });
  }
}
