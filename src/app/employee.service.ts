import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Employee } from './dto';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private readonly http: HttpClient) { }

  load(id: number): Observable<Employee> {
    return this.http
      .get(`../api/employee/${encodeURIComponent(id.toString())}`)
      .pipe(map(json => json as Employee));
  }

  listAll(): Observable<Employee[]> {
    return this.http.get('../api/employees').pipe(
      map(json => json as Employee[])
    );
  }

  save(emp: Employee): Observable<{ id: string }> {
    if (emp.id) {
      /* update */
      return this.http
        .put(`../api/employee/${encodeURIComponent(emp.id.toString())}`, emp)
        .pipe(map(json => json as { id: string }));
    } else {
      /* create */
      return this.http
        .post('../api/employee', emp)
        .pipe(map(json => json as { id: string }));
    }
  }
}
