import { Pipe, PipeTransform } from '@angular/core';
import { CommunicationCounts, Channel } from './dto';

@Pipe({
  name: 'communicationVerbose',
  pure: false
})
export class CommunicationVerbosePipe implements PipeTransform {
  transform(cc: CommunicationCounts, channel?: Channel): string {
    return (
      `${cc.recipient.firstName} ${cc.recipient.lastName} received ` +
      (channel ? this.ofChannel(cc, channel) :
      `${this.ofChannel(cc, Channel.EMAIL)} and ${this.ofChannel(cc, Channel.PHONE)}`)
    );
  }

  private ofChannel(cc: CommunicationCounts, ch: Channel): string {
    let verb;
    switch (ch) {
      case Channel.EMAIL: verb = 'emails'; break;
      case Channel.PHONE: verb = 'phone calls'; break;
    }
    return `${cc.counts[ch]} ${verb}`;
  }
}
