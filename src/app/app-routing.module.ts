import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeWizComponent } from './views/employee-wiz/employee-wiz.component';
import { EmployeeResolverService } from './views/employee/employee-resolver.service';
import { EmployeeComponent } from './views/employee/employee.component';
import { EmployeesComponent } from './views/employees/employees.component';
import { EmployeeEditorGuard } from './employee-editor.guard';

const routes: Routes = [
  {
    path: 'employees',
    component: EmployeesComponent
  },
  {
    path: 'employee/wizard',
    component: EmployeeWizComponent,
    canActivate: [EmployeeEditorGuard]
  },
  {
    path: 'employee/:id',
    component: EmployeeComponent,
    resolve: { employee: EmployeeResolverService }
  },
  {
    path: '**',
    redirectTo: '/employees'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
