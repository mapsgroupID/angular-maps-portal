import { Component } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee, Position } from '../dto';
import { NgForm, FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { validateInteger, validateGt } from './validators';

function mkBlankEmployee(): Employee {
  return {
    id: null,
    avatarUrl: null,
    firstName: null,
    lastName: null,
    position: null,
    description: null,
    age: 0,
    hasEmail: false,
    hasPhone: false
  };
}

@Component({
  selector: 'app-employee-editor',
  templateUrl: './employee-editor.component.html',
  styleUrls: ['./employee-editor.component.css']
})
export class EmployeeEditorComponent {
  readonly form: FormGroup;
  employee: Employee;
  positions: Position[] = [Position.DEV, Position.ARCH, Position.PM, Position.RES, Position.CTO];

  constructor(
    private readonly fb: FormBuilder,
    private readonly empService: EmployeeService) {

    this.form = this.fb.group({
      firstName: this.fb.control(null, [Validators.required]),
      lastName: this.fb.control(null, [Validators.required]),
      avatarUrl: this.fb.control(null, [Validators.required, Validators.maxLength(200)]),
      description: this.fb.control(null, [Validators.maxLength(200)]),
      position: this.fb.control(null, [Validators.required]),
      age: this.fb.control(null, [Validators.required, validateInteger, validateGt(15)]),
      hasEmail: this.fb.control(null, []),
      hasPhone: this.fb.control(null, []),
    });
  }

  get firstName(): AbstractControl { return this.form.get('firstName'); }
  get lastName(): AbstractControl { return this.form.get('lastName'); }
  get avatarUrl(): AbstractControl { return this.form.get('avatarUrl'); }
  get description(): AbstractControl { return this.form.get('description'); }
  get position(): AbstractControl { return this.form.get('position'); }
  get age(): AbstractControl { return this.form.get('age'); }
  get hasEmail(): AbstractControl { return this.form.get('hasEmail'); }
  get hasPhone(): AbstractControl { return this.form.get('hasPhone'); }

  private mkEmployee(): Employee {
    return {
      id: null,
      avatarUrl: this.avatarUrl.value,
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      position: this.position.value,
      description: this.description.value,
      age: this.age.value,
      hasEmail: this.hasEmail.value,
      hasPhone: this.hasPhone.value
    };
  }

  save(f: NgForm) {
    this.form.disable();
    this.empService.save(this.mkEmployee())
      .toPromise()
      .then(out => {
        console.log(`Employee saved: ${out.id}`);
        this.form.enable();
        f.resetForm();
      }).catch(err => {
        console.log('An error occurred', err);
        this.form.enable();
      });
  }

}
