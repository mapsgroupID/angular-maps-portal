import { Component } from '@angular/core';
import { AuthorizationService } from './authorization.service';
import { MatSlideToggleChange } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private readonly auth: AuthorizationService) { }

  get canCreateEmployees() {
    return this.auth.canI('create-employee');
  }

  onChange(event: MatSlideToggleChange) {
    this.auth.patch({ 'create-employee': event.checked });
  }
}
