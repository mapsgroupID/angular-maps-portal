import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  private acl: { [permission: string]: boolean };

  constructor() {
    this.acl = {};
  }

  patch(diff: { [permission: string]: boolean }) {
    this.acl = Object.assign(this.acl, diff);
  }

  canI(doWhat: string): boolean {
    return this.acl[doWhat] === true;
  }
}
